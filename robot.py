class Robot():
    def __init__(self, name, location, graphRobot, locInfo, energy = 100 ):
        self.name = name
        self.location = location #string
        self.energy = energy
        self.graph = graphRobot


    #getter and setters

    def get_name(self):
        return self.name

    def get_location(self):
        return self.location

    def get_energy(self):
        return self.energy

    def set_location(location):
        self.location = location

    def set_energy():
        pass

    #Actions

    #moves from current location to destination if they are connected and returns true,
    #otherwise returns False
    def moveTo(self, destination):
        if (destination in self.graph[self.location]):
            self.location = destination
            self.energy = self.energy - 1
            return True
        return False

    #returns True if dust is at current location, otherwis return False
    def sense_dust(self):
        pass

    #returns True if dust is at current location and updates location info, otherwise returns False
    def vaccum(self):
        pass
