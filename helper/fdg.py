import math
import random
from baseClasses.vector import Vector

c1 = 2
c2 = 1
c3 = 1
c4 = 0.1


def getNetForce(nodeDict, graph):
        netForce={}
        for nodeName in nodeDict:
            #find net attractive force experienced by nodeName
            attrForce = getNetAttrForce(nodeDict[nodeName], graph[nodeName])

            #find nodes not connected to nodeName and calculate net repulisve foce
            nonNeighbours = [x for x in nodeDict.keys() if x not in [y.name for y in graph[nodeName]] and x!=nodeName ]

            repForce= getNetRepForce(nodeDict[nodeName], [nodeDict[x] for x in nonNeighbours])

            #add up the attractive and repulsive force
            totForce = Vector.add1(attrForce, repForce)
            #attrForce.add(repForce)
            #print nodeName, "ahem:", attrForce, ",", repForce
            #add random component
            attrForce.normalize()
            repForce.normalize()

            # if ( abs(attrForce.mag()) <0.001 and  abs(repForce.mag() < 0.001)):
            #     attrForce.add(Vector(random.uniform(-100,100), random.uniform(-100,100)) )
            # if (random.uniform(0,1)>0.9):
            #     totForce.add(Vector(random.uniform(-0.1,1), random.uniform(-1,1)) )

            netForce[nodeName] = totForce

        return netForce


#find netAttractive
def getNetAttrForce(u, nodeList):
    f=Vector(0,0)
    for v in nodeList:
        f.add(getAttrForce(u.loc, v.loc))
    return f



#get attractive force between nodes
#c1 * math.log10()
def getAttrForce(u, v):
    dirVector = Vector.getDirVector(u,v)
    dirVector.normalize()
    mag = c1 * math.log10(dirVector.mag()/c2)
    dirVector.mult(1*mag)

    return dirVector
    #return dirVector


#find net repulsive force
def getNetRepForce(u, nodeList):
    f=Vector(0,0)
    for v in nodeList:
        f.add(getRepForce(u.loc, v.loc))
    return f


#get attractive force between nodes
#c3 * d2
def getRepForce(u, v):
    dirVector = Vector.getDirVector(u,v)
    dirVector.normalize()
    dist = dirVector.mag()
    if (dist>0):
        mag = c3 / (dist * dist)
    else:
        mag = c3;
    dirVector.mult(-0.5*mag)
    return dirVector

#get non-neighbours
def getNonNeighbours(nodeName, neighbours, nodeList):
    return [x for x in nodeList if x not in neighbours]
