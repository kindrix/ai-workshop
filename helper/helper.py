import sys
import io
from baseClasses.vector import Vector
from baseClasses import drawableNode
import random


#return a dicionary with nodeName as key. the value is a list conisting of a tuple of the nodes coordinates if any, and a list of the names of the neighbouring nodes

def readData():
    fname = str(sys.argv [1])
    with open(fname) as fo:
        #save graph information temporarily
        nodeData = {}

        #check if coord are to be randomly assigned
        temp = fo.readline()
        temp  = (temp.split("=")[1]).strip()
        if (temp == "True"):
            randomCoords = True

        #read node data
        for line in fo:
            fields = line.split(":")
            #get node name
            fieldLeft =  fields[0].split(" ")
            nodeName = fieldLeft[0]
            x  = float(fieldLeft[1])
            y =  float(fieldLeft[2])
            #save node data
            nodeData[nodeName] = [(x, y)]
            #get adjacent nodes
            neighbours = fields[1][1:].split(" ")
            neighbours = [a.strip() for a in neighbours]
            nodeData[nodeName].append(neighbours)
    return (nodeData,randomCoords)


#create graph
def createGraph(nodeData, randomCoords, RAD, WIDTH, HEIGHT ):

    nodeDict = {}
    graph = {}

    #create drawableNodes
    if (randomCoords):
        for nodeName in nodeData:
            nodeDict[nodeName] = drawableNode.DrawableNode(nodeName, RAD, Vector(random.randrange(RAD, WIDTH),random.randrange(RAD, HEIGHT)))
    else:
        for nodeName in nodeData:
            x = nodeData[nodeName][0][0]
            y = nodeData[nodeName][0][1]
            nodeDict[nodeName] = drawableNode.DrawableNode(nodeName, RAD, Vector(x, y))

    #create graph
    for nodeName in nodeData:
        graph[nodeName] = [nodeDict[x] for x in nodeData[nodeName][1]]

    return (graph, nodeDict)
