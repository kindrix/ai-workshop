#!/usr/bin/python
from Tkinter import *
from helper import helper
import gui
import robot

WIDTH = 600
HEIGHT = 400
RAD = 15

#read data
(nodeData, randomCoords)= helper.readData()

#create graph for robot - consist of string names
#{'a':['b]', 'c':['d','e']}
graphRobot = {x:nodeData[x][1] for x in nodeData}


#create graph
(graph, nodeDict) = helper.createGraph(nodeData, randomCoords, RAD, WIDTH, HEIGHT)

tk = Tk()
#tk.minsize(800, 600)
robot = robot.Robot("Kin", "a", graphRobot, graphRobot, energy = 100)
gui = gui.GUI( robot, tk, graph, nodeDict, WIDTH, HEIGHT, showNodeName=True)
tk.mainloop()
