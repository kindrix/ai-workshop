#gui class
from Tkinter import *
from baseClasses.drawableNode import DrawableNode
from helper import fdg
import random
import thread


class GUI():
    def __init__(self, agent, master, graph, nodeDict, WIDTH=600, HEIGHT=600, showNodeName=True):
        self.master = master
        self.graph = graph
        self.nodeDict = nodeDict
        self.agent = agent
        self.WIDTH = WIDTH
        self.HEIGHT = HEIGHT
        self.showNodeName = showNodeName
        #used for triggering events when node selected in options menu
        self.vars=""
        self.setUpGui(master)
        self.setUpGraph(master, graph, nodeDict, WIDTH, HEIGHT)
        self.oldRobotLocation = agent.get_location()
        self.updateLocationColor()
        self.logCount=1
        self.logMessage("Initialisation complete..")

    #set up gui
    def setUpGui(self, master):
        master.title("Comp329 : File Name")

        #top Frame
        self.frame_top = Frame(master, bg="white", bd=1, relief="sunken")
        self.frame_top.pack(fill=BOTH, expand=1, side=TOP)

        #bottomFrame
        self.frame_bottom = Frame(master, width=800, height=150, bd=1, relief="sunken")
        self.frame_bottom.pack(fill=BOTH, expand=1)

        #footerFrame
        self.frame_footer = Frame(master, width=800, height=10, bd=1)
        self.frame_footer.pack(fill=BOTH, expand=1)

        #top frame -> canvas
        # self.canvas= Canvas(self.frame_top, width=600, height=500, bg="#d3d3d3")
        # self.canvas.pack(fill=BOTH, expand=1)

        #bottom frame -> left column
        self.frame_bottom_left = Frame(self.frame_bottom, width = 200, height=150, bd=1, relief="sunken")
        self.frame_bottom_left.pack(side=LEFT, fill=Y, expand=0)

        #bottom frame -> middle column
        self.frame_bottom_middle = Frame(self.frame_bottom, width = 300, height=150, bd=1, relief="sunken")
        self.frame_bottom_middle.pack(side=LEFT, fill=BOTH, expand=1)

        #bottom frame -> right column
        self.frame_bottom_right = Frame(self.frame_bottom, width = 250, height=150, bd=1, relief="sunken")
        self.frame_bottom_right.pack(side=RIGHT, expand=1, fill=BOTH)

        # #bottom frame -> left column -> robot image
        photoRobot = PhotoImage(file="res/robot.gif")
        self.robot_photo = Label(self.frame_bottom_left, image=photoRobot, width=200,height=100, pady=10)
        self.robot_photo.image = photoRobot # keep a reference!
        self.robot_photo.grid(row=0)

        #bottom frame -> left column -> robot name
        self.lbl_robot_name = Label(self.frame_bottom_left, text="Name: " + self.agent.name, padx=20)
        self.lbl_robot_name.grid(row=1, sticky =W)

        #bottom frame -> left column -> robot location
        self.lbl_robot_location = Label(self.frame_bottom_left, text="Location:", padx=5)
        self.lbl_robot_location.grid(row=2, sticky =W)

        #bottom frame -> left column -> robot location
        self.lbl_robot_energy = Label(self.frame_bottom_left, text="Energy:", padx=15)
        self.lbl_robot_energy.grid(row=3, sticky =W)

        #bottom frame -> middle column -> action name
        self.frame_bottom_middle.grid_columnconfigure(1, weight=1)


        self.lbl_actions = Label(self.frame_bottom_middle, text="Available Actions", pady = 5)
        self.lbl_actions.grid(row=0, sticky = E+W)

        #bottom frame -> middle column -> move to node

        options = [x for x in self.nodeDict]
        options.sort()
        options.insert(0, "Move to:")
        var = StringVar(self.frame_bottom_middle)
        var.set(options[0]) # initial value
        var.trace("w", lambda n1, n2, op, nodeName=var:self.guiMoveTo(nodeName,n1, n2, op))
        w = apply(OptionMenu, (self.frame_bottom_middle, var) + tuple(options))
        w.grid(row=1, column=0 )


        #bottom frame -> middle column -> sense dust
        self.btn_sense_dust = Button(self.frame_bottom_middle, text="Sense Dust")
        self.btn_sense_dust.grid(row = 1, column = 1)

        #bottom frame -> middle column -> vacuum
        self.btn_sense_dust = Button(self.frame_bottom_middle, text="Vacuum")
        self.btn_sense_dust.grid(row = 1, column = 2)



        #bottom frame -> right column -> log

        self.scrollbar = Scrollbar(self.frame_bottom_right)
        self.scrollbar.pack(side=RIGHT, fill=Y)

        self.log = Text(self.frame_bottom_right,  bd=1, relief="sunken",
        padx=2, pady=2, yscrollcommand=self.scrollbar.set, height=10, width = 30)
        self.log.insert(END, "Messaage Log\n-------------\n")
        self.log.config(state=DISABLED)
        self.scrollbar.config(command=self.log.yview)
        self.log.pack(expand=1, fill=BOTH)

    #set up canvas
    def setUpGraph(self, master, graph, nodeDict, WIDTH, HEIGHT):
        self.master.bind_all('<KeyPress>', self.keyPressedFDG)

        self.canvas = Canvas(self.frame_top, width  = WIDTH, height = HEIGHT)
        self.canvas.pack()

        #draw initial nodes
        for u in nodeDict.values():
            DrawableNode.drawNode(u, self.canvas, self.showNodeName)

        #draw edges
        for nodeName in nodeDict:
            for node in graph[nodeName]:
                DrawableNode.drawEdge(nodeDict[nodeName], node, self.canvas)

        #bind node drag events
        for u in nodeDict.values():
            self.canvas.tag_bind(u.oval, '<B1-Motion>', lambda event, obj=u, c = self.canvas: self.dragNode(event, obj, c))

    #update color of current location to blue
    def updateLocationColor(self):
        u = self.nodeDict[self.oldRobotLocation]
        self.canvas.itemconfig(u.tag, fill = "yellow")
        u = self.nodeDict[self.agent.location]
        self.canvas.itemconfig(u.tag, fill = "#ff0000")
        self.oldRobotLocation = self.agent.location
        self.lbl_robot_location.config( text = "Location: " + self.agent.location)
        self.lbl_robot_energy.config(text = "Energy: " + str(self.agent.energy))


    #update gui current node
    def guiMoveTo(self, var, *args):
        dest = var.get()
        u = self.nodeDict[self.agent.location]
        if (self.agent.moveTo(dest)):
            v = self.nodeDict[self.agent.location]
            thread.start_new_thread(DrawableNode.flashLineColor, (u, v, self.canvas))
            self.updateLocationColor()
            self.logMessage("OK: Moved to " + dest)
            return
        self.logMessage("Fail: Move to " + dest)

    #event method for moving node
    def dragNode(self,event, u, canvas):
        deltaX = event.x - u.loc.x
        deltaY = event.y - u.loc.y
        DrawableNode.moveNode(u, self.graph[u.name], canvas, deltaX, deltaY)

    #event method to applyFDG if key is pressed
    def keyPressedFDG(self, event):
        key = str(event.char)
        #s is for short
        if (key == 's' or key =='S'):
            self.logMessage("OK: Redrawing graph")
            self.applyFDG(5)
        #l is for long
        elif (key == 'l' or key =='L'):
            self.logMessage("OK: Redrawing graph")
            self.applyFDG(15)

    #log Message
    def logMessage(self, msg):
        self.log.config(state=NORMAL)
        #oldMsg = self.log.get(1.0, END)
        self.log.insert(END, str(self.logCount) + ". " + msg + "\n")
        self.logCount = self.logCount + 1
        self.log.config(state=DISABLED)
        self.log.see("end")

    #turn on fdg
    def applyFDG(self, runFactor):
        #------- eades -----------------
        fdgOn = True;
        if fdgOn == True:
            #copy old locs
            tempNodeList={}
            for u in self.nodeDict.values():
                tempNodeList[u.name] = DrawableNode.getSimpleCopy(u)
            # for u in nodeDict.values():
            #     print "old:",u.loc, ", new:", tempNodeList[u.name].loc
            for i in range(len(self.nodeDict)*runFactor):
                #time.sleep(.001)
                #print "------loop----"
                netForce=fdg.getNetForce(self.nodeDict, self.graph)
                #print "netforce:", netForce
                for nodeName in tempNodeList:
                    u = tempNodeList[nodeName]
                    u.applyForce(netForce[u.name])
                    u.updateLocation()
                    # print "old:", netForce
                DrawableNode.scaleToCanvas(tempNodeList.values(), self.WIDTH, self.HEIGHT)
                for u in self.nodeDict.values():
                    v = tempNodeList[u.name]
                    deltaX = v.loc.x - u.loc.x
                    deltaY = v.loc.y - u.loc.y
                    DrawableNode.moveNode(u, self.graph[u.name], self.canvas, deltaX, deltaY )

                self.master.update()
        #------- eades -----------------
